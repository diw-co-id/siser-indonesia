<?
include("connect_server.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php include("copyright.php"); ?>
<html lang="id" itemscope itemtype="http://schema.org/WebPage" xmlns="http://www.w3.org/1999/xhtml" xml:lang="id">
	<head>
		<title>FAQ | <?php echo"$row_setting[title]"; ?></title>
		
		<?php include("meta.php"); ?>
	</head>
	<body>
		
		<?php include("header.php"); ?>
	    
    	<section id="about-us" style="margin-top:50px;">
        	<div class="container">
    			<div class="col-sm-12">
    				<center><h2 class="title-one text-center">FAQ</h2></center>
					<div class="panel-group" id="accordion">
                		<div class="panel panel-default">
                			<div class="panel-heading">
                				<h4 class="panel-title" style="padding:10px; margin-bottom:0px;">
                					<a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Kemasan</a>
                				</h4>
                			</div>
                			<div id="collapse1" class="panel-collapse collapse in">
                				<div class="panel-body">
                				    Bagaimana cara pengemasan agar kondisi terjaga baik selama pengiriman?<br>
        						    <br>
                                    &rsaquo; Produk mesin akan di kemas dengan rapih dan aman.
            					</div>
            				</div>
            			</div>
            			<div class="panel panel-default">
            				<div class="panel-heading">
            				    <h4 class="panel-title" style="padding:10px; margin-bottom:0px;">
            						<a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Harga</a>
            					</h4>
            				</div>
            				<div id="collapse2" class="panel-collapse collapse">
            					<div class="panel-body">
            					    Berapa harga produk-produk di website siser Indonesia?<br>
        						    <br>
                                    &rsaquo; Lebih lengkapnya dapat langsung di tanyakan kepada marketing staff kami di nomor (6221) 633-7888 dan email siser@centralsps.com. Anda juga bisa menghubungi kami melalui kolom kontak di website kami. Staff marketing kami akan menghubungi Anda.
        						</div>
        					</div>
        				</div>
        				<div class="panel panel-default">
        					<div class="panel-heading">
        				        <h4 class="panel-title" style="padding:10px; margin-bottom:0px;">
        							<a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Katalog Produk</a>
        						</h4>
        					</div>
        					<div id="collapse3" class="panel-collapse collapse">
        						<div class="panel-body">
        						    Apakah saya bisa mendapatkan katalog produk? Dan permintaan katalog dapat langsung dari siapa?<br>
        						    <br>
                                    &rsaquo; Iya, bisa. Permintaan katalog dapat langsung ditanyakan kepada marketing staff kami di nomor (6221) 633-7888 dan email siser@centralsps.com. Anda juga bisa menghubungi kami melalui kolom kontak di website kami. Staff marketing kami akan menghubungi Anda.
    							</div>
    						</div>
    					</div>
        				<div class="panel panel-default">
        					<div class="panel-heading">
        				        <h4 class="panel-title" style="padding:10px; margin-bottom:0px;">
        							<a data-toggle="collapse" data-parent="#accordion" href="#collapse4">Sistem Pembayaran</a>
        						</h4>
        					</div>
        					<div id="collapse4" class="panel-collapse collapse">
        						<div class="panel-body">
        						    Jenis pembayaran apa yang dapat diterima?<br>
        						    <br>
                                    &rsaquo; Kami menerima sistem pembayaran COD. Atau dengan mentransfer ke rekening kami PT. Central Satrya Perdana, BCA Islamic A/c 8840 0301 90
 
    							</div>
    						</div>
    					</div>
        				<div class="panel panel-default">
        					<div class="panel-heading">
        				        <h4 class="panel-title" style="padding:10px; margin-bottom:0px;">
        							<a data-toggle="collapse" data-parent="#accordion" href="#collapse5">Tutorial</a>
        						</h4>
        					</div>
        					<div id="collapse5" class="panel-collapse collapse">
        						<div class="panel-body">
        						    Apakah saya bisa mendapatkan tutorial penggunaan produk?<br>
        						    <br>
                                    &rsaquo; Iya, bisa.  Kami akan dengan senang hati melayani anda dan memberikan konsultasi tekhnik yang diperlukan untuk meningkatkan kualitas printing sesuai kebutuhan customer kami.
    							</div>
    						</div>
    					</div>
        				<div class="panel panel-default">
        					<div class="panel-heading">
        				        <h4 class="panel-title" style="padding:10px; margin-bottom:0px;">
        							<a data-toggle="collapse" data-parent="#accordion" href="#collapse6">Pengiriman</a>
        						</h4>
        					</div>
        					<div id="collapse6" class="panel-collapse collapse">
        						<div class="panel-body">
        						    Berapa waktu pengiriman hingga sampai tujuan? Dan kapan barang dikirim setelah ada konfirmasi pembayaran?<br>
        						    <br>
                                    &rsaquo; Lama pengiriman barang tergantung pada jarak tujuan dari Jakarta dan jenis moda pengiriman yang dipilih oleh Anda sesuai kesepakatan dengan marketing kami pengiriman dengan menggunakan jenis apa, dan barang akan dikirim setelah ada konfirmasi secepat mungkin.
    							</div>
    						</div>
    					</div>
                	</div>
        		</div>
    		</div>
    	</section>
		
    	<?php include("footer.php"); ?>
    </body>
</html>