<?
include("connect_server.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php include("copyright.php"); ?>
<html lang="id" itemscope itemtype="http://schema.org/WebPage" xmlns="http://www.w3.org/1999/xhtml" xml:lang="id">
	<head>
		<title>Hubungi Kami | <?php echo"$row_setting[title]"; ?></title>
		
		<?php include("meta.php"); ?>
	</head>
	<body>
		
		<?php include("header.php"); ?>
	    
    	<section id="contact" style="margin-top:50px;">
    		<div class="container">
    			<div class="row text-center clearfix">
    				<div class="col-sm-8 col-sm-offset-2">
    					<div class="contact-heading">
    						<h2 class="title-one">Hubungi Kami</h2>
    						<p>Hubungi Kami Jika Ada Pertanyaan Dan Keluhan Anda</p>
    					</div>
    				</div>
    			</div>
    		</div>
    		<div class="container">
    			<div class="contact-details">
    				<!--<div class="pattern"></div>-->
    				<div class="row text-center clearfix">
    					<div class="col-sm-6">
    						<div class="contact-address">
    						    <address>
    						        <p><span>Kantor</span> Pusat</p>
    						        <strong>
                                        Ketapang Indah Blok A - 1 No, 3-4<br>
                                        Jl. KH. Zainul Arifin Jakarta 11140, Indonesia<br>
                                        Telp: (6221) 633 7888, Fax: (6221) 631 7888, 632 7888<br>
                                        No. Hp / Whatsapp : 0811 1792 599 <br>
                                        Email: siser@centralsps.com<br>
                                        <br>
                                        Jam Kerja:<br>
                                        Senin-Jumat: 08:00-17:00<br>
                                        Sabtu: 08:00-15:00
						            </strong>
						            <br>
					            </address>
    							<div class="social-icons">
    								<a draggable="false" href="<?php echo"$row_setting[facebook]"; ?>"><i class="fa fa-facebook"></i></a>
    								<a draggable="false" href="<?php echo"$row_setting[instagram]"; ?>"><i class="fa fa-instagram"></i></a>
    							</div>
    						</div>
    					</div>
    					<div class="col-sm-6"> 
    						<!--<div id="contact-form-section">
    							<div class="status alert alert-success" style="display: none"></div>
    							<form id="contact-form" class="contact" name="contact-form" method="post" action="send-mail.php">
    								<div class="form-group">
    									<input type="text" name="name" class="form-control name-field" required="required" placeholder="Your Name">
									</div>
									<div class="form-group">
										<input type="email" name="email" class="form-control mail-field" required="required" placeholder="Your Email">
									</div> 
									<div class="form-group">
										<textarea name="message" id="message" required="required" class="form-control" rows="8" placeholder="Message"></textarea>
									</div> 
									<div class="form-group">
										<button type="submit" class="btn btn-primary">Send</button>
									</div>
								</form> 
							</div>-->
							<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1088.6634570975289!2d106.8177522!3d-6.1597383!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f5df62dd5943%3A0xd65cafdacf98200f!2sKetapang+Indah%2C+Jl.+Kyai+H.+Zainul+Arifin%2C+Krukut%2C+Tamansari%2C+Kota+Jakarta+Barat%2C+Daerah+Khusus+Ibukota+Jakarta+11140!5e1!3m2!1sid!2sid!4v1512124644636" width="100%" height="390" frameborder="0" style="border:0" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>
		</section>
		
    	<?php include("footer.php"); ?>
    </body>
</html>