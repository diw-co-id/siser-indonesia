<?
include("connect_server.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php include("copyright.php"); ?>
<html lang="id" itemscope itemtype="http://schema.org/WebPage" xmlns="http://www.w3.org/1999/xhtml" xml:lang="id">
	<head>
		<title>Tentang Siser Indonesia | <?php echo"$row_setting[title]"; ?></title>
		
		<?php include("meta.php"); ?>
	</head>
	<body>
		
		<?php include("header.php"); ?>
	    
    	<section id="about-us" style="margin-top:50px;">
        	<div class="container">
        		<div class="text-center">
        			<div class="col-sm-8 col-sm-offset-2">
        				<h2 class="title-one">Mengapa Siser Indonesia?</h2>
        				<img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/logo-siser-indonesia.png" width="300" alt="Logo <?php echo"$row_setting[name_website]"; ?> - <?php echo"$row_setting[title]"; ?>"><br><br><br>
        				<p>Siser Indonesia adalah website screen printing di indonesia yang menyediakan Cutting Material dan Printing Material dengan produk terbaik dan kualitas teratas untuk memenuhi kebutuhan para pencetak sablon di berbagai aplikasi dan industri seperti PCB, Elektronik, Grafik/Poster, Garmen/Tekstil, Plastik, Keramik transfer/decal, Stiker, Sepatu, Spanduk, dll.</p>
        			</div>
        		</div>
    		</div>
    	</section>
        
		<section id="portfolio" style="background-color:#FFFFFF;">
			<div class="container">
				<div class="row text-center">
					<div class="col-sm-8 col-sm-offset-2">
						<h2 class="title-one">Galeri Siser Indonesia</h2>
						<p>Berikut ini adalah galeri Siser Indonesia</p>
					</div>
				</div>
				<div class="portfolio-items">
				<?
				$a=1;
				while($a <= 21)
				{
				?>
					<div class="col-sm-3 col-xs-12 portfolio-item html">
						<div class="portfolio-image">
							<img src="images/galeri/galeri-<?php echo"$a"; ?>.jpg" alt="">
						</div>
					</div>
				<?
				    $a++;
				}
				?>
				</div>
			</div> 
		</section>
		
    	<?php include("footer.php"); ?>
    </body>
</html>