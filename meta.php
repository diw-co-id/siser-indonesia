		<meta charset="UTF-8" />
		<base href="<?php echo"$row_setting[domain]"; ?>/" />

	<? if($row_setting[refresh] != '') { ?>
		<meta http-equiv="refresh" content="<?php echo"$row_setting[refresh]"; ?>" />
	<? } ?>

		<!-- Mobile Specific Metas -->
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
		<meta name="format-detection" content="telephone=no">
		<!-- Mobile Specific Metas -->

		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="Content-Language" content="all-language">
		<meta http-equiv="Content-Script-Type" content="text/javascript" />
		<meta http-equiv="Content-Style-Type" content="text/css" />
		<meta http-equiv="Pragma" content="no-cache" />
		<meta http-equiv="Expires" content="never" />
		<meta http-equiv="Cache-Control" content="no-cache" />
		<meta http-equiv="imagetoolbar" content="no" />
		<meta http-equiv="X-UA-Compatible" content="IE=8, IE=9, IE=10, IE=edge, chrome=1" >
		<meta http-equiv="CHARSET" content="ISO-8859-1">
		<meta http-equiv="VW96.OBJECT TYPE" content="Document">

		<!-- URL Theme Color untuk Chrome, Firefox OS, Opera dan Vivaldi -->
		<meta name="theme-color" content="#FFFFFF" />

		<!-- URL Theme Color untuk iOS Safari -->
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<meta name="apple-mobile-web-app-status-bar-style" content="#FFFFFF" />
		<meta name="apple-itunes-app" content="app-id=4374428654">

		<meta name="msapplication-window" content="width=device-width;height=device-height" />
		<meta name="msapplication-task" content="name=<?php echo"$row_setting[title]"; ?>; action-uri=<?php echo"$row_setting[domain]"; ?>/; icon-uri=<?php echo"$row_setting[domain]"; ?>/images/<?php echo"$row_setting[favicon]"; ?>">
		<meta name="msapplication-TileImage" content="<?php echo"$row_setting[domain]"; ?>/images/<?php echo"$row_setting[logo]"; ?>">
		<meta name="msapplication-TileColor" content="#FFFFFF">
		<meta name="msapplication-navbutton-color" content="#FFFFFF" />
		<meta name="msApplication-PackageFamilyName" content="<?php echo"$row_setting[title]"; ?>">
		<meta name="msApplication-ID" content="App">

		<meta name="if:Sliding header" content="1">
		<meta name="if:Show navigation" content="1">
		<meta name="if:Endless scrolling" content="1">
		<meta name="if:Syntax highlighting" content="0">
		<meta name="select:Layout" content="regular" title="Regular">
		<meta name="select:Layout" content="narrow" title="Narrow">
		<meta name="select:Layout" content="grid" title="Grid">
		<meta name="if:Related Posts" content="1">
		<meta name="text:Disqus shortname" content="<?php echo"$row_setting[name_website]"; ?>">
		<meta name="text:Google analytics ID" content="<?php echo"$row_setting[google_analytics_id]"; ?>">

		<meta name="name" content="<?php echo"$row_setting[name]"; ?>" />
		<meta name="title" content="<?php echo"$row_setting[title]"; ?>" />
		<meta name="description" content="<?php echo"$row_setting[description]"; ?>" />
		<meta name="subject" content="<?php echo"$row_setting[subject]"; ?>" />
		<meta name="abstract" content="<?php echo"$row_setting[abstract]"; ?>" />
		<meta name="application-name" content="<?php echo"$row_setting[application_name]"; ?>">

		<meta itemprop="name" content="<?php echo"$row_setting[itemprop_name]"; ?>">
		<meta itemprop="description" content="<?php echo"$row_setting[itemprop_description]"; ?>">
		<meta itemprop="image" content="<?php echo"$row_setting[domain]"; ?>/images/<?php echo"$row_setting[itemprop_image]"; ?>">

		<meta name="keywords" content="<?php echo"$row_setting[keywords]"; ?>, <?php echo"$row_setting[norton_safeweb_site_verification]"; ?>, <?php echo"$row_setting[wot_verification]"; ?>" />

		<meta name="designer" content="<?php echo"$row_setting[designer]"; ?>" />
		<meta name="creator" content="<?php echo"$row_setting[creator]"; ?>" />
		<meta name="publisher" content="<?php echo"$row_setting[publisher]"; ?>" />
		<meta name="author" content="<?php echo"$row_setting[author]"; ?>" />
		<meta name="rights" content="<?php echo"$row_setting[rights]"; ?>" />

		<meta name="referrer" content="<?php echo"$row_setting[origin]"; ?>">
		<meta name="language" content="<?php echo"$row_setting[language]"; ?>" />
		<meta name="aesop" content="<?php echo"$row_setting[information]"; ?>" />
		<meta name="target" name="<?php echo"$row_setting[target]"; ?>" />
		<meta name="revisit" content="<?php echo"$row_setting[revisit]"; ?>" />
		<meta name="revisit-after" content="<?php echo"$row_setting[revisit_after]"; ?>" />

		<meta name="webcrawlers" content="all" />
		<meta name="rating" content="all" />
		<meta name="spiders" content="all" />
		<meta name="robots" content="all" />
		<meta name="audience" content="all" />
		<meta name="copyright" content="<?php echo"$row_setting[copyright]"; ?>" />

		<meta name="formatter" content="<?php echo"$row_setting[formatter]"; ?>">

		<meta name="generator" content="<?php echo"$row_setting[generator]"; ?>" />
		<meta name="generator" content="DrewBull-Q" />
		<meta name="generator" content="WordPress 4.2.9" />
		<meta name="generator" content="Blogger" />
		<meta name="generator" content="Dreamweaver">
		<meta name="generator" content="EditPlus">
		<meta name="generator" content="Frontpage">
		<meta name="generator" content="NetObjects Fusion 4.0 for Windows">
		<meta name="generator" content="FrontPage 4.0">
		<meta name="generator" content="WordPress">
		<meta name="generator" content="Joomla!">
		<meta name="generator" content="Wordpress 2.x.x">
		<meta name="generator" content="Macromedia Dreamweaver">
		<meta name="generator" content="Joomla! - Copyright Open Source Matters. All rights reserved." />
		<meta name="generator" content="Microsoft FrontPage 6">

		<meta name="alexaVerifyID" content="<?php echo"$row_setting[alexa_verify_id]"; ?>" />
		<meta name="google-site-verification" content="<?php echo"$row_setting[google_site_verification]"; ?>" />
		<meta name="p:domain_verify" content="<?php echo"$row_setting[p_domain_verify]"; ?>"/>
		<meta name="msvalidate.01" content="<?php echo"$row_setting[msvalidate_01]"; ?>" />
		<meta name="norton-safeweb-site-verification" content="<?php echo"$row_setting[norton_safeweb_site_verification]"; ?>" />
		<meta name="wot-verification" content="<?php echo"$row_setting[wot_verification]"; ?>" />
		<meta name="avgthreatlabs-verification" content="<?php echo"$row_setting[avgthreatlabs_verification]"; ?>" />

		<meta property="fb:pages" content="<?php echo"$row_setting[fb_pages]"; ?>">
		<meta property="fb:app_id" content="<?php echo"$row_setting[fb_app_id]"; ?>" />
		<meta property="fb:page_id" content="<?php echo"$row_setting[fb_page_id]"; ?>" />
		<meta property="fb:admins" content="<?php echo"$row_setting[fb_admins]"; ?>" />
		<meta property="twitter:account_id" content="<?php echo"$row_setting[twitter_account_id]"; ?>" />
		<meta property="article:publisher" content="<?php echo"$row_setting[article_publisher]"; ?>" />

		<meta property="og:type" content="<?php echo"$row_setting[og_type]"; ?>" />
		<meta property="og:url" content="<?php echo"$row_setting[og_url]"; ?>" />
		<meta property="og:title" content="<?php echo"$row_setting[og_title]"; ?>" />
		<meta property="og:site_name" content="<?php echo"$row_setting[og_site_name]"; ?>" />
		<meta property="og:description" content="<?php echo"$row_setting[og_description]"; ?>" />
		<meta property="og:image" content="<?php echo"$row_setting[domain]"; ?>/images/<?php echo"$row_setting[og_image]"; ?>"/>

		<meta name="DC.title" content="<?php echo"$row_setting[dc_title]"; ?>" />
		<meta name="DC.creator" content="<?php echo"$row_setting[dc_creator]"; ?>" />
		<meta name="DC.publisher" content="<?php echo"$row_setting[dc_publisher]"; ?>" />
		<meta name="DC.type" scheme="DCTERMS.DCMIType" content="<?php echo"$row_setting[dc_type]"; ?>" />
		<meta name="DC.format" content="<?php echo"$row_setting[dc_format]"; ?>" />
		<meta name="DC.identifier" scheme="DCTERMS.URI" content="<?php echo"$row_setting[dc_identifier]"; ?>" />

		<meta name="geo.region" content="<?php echo"$row_setting[geo_region]"; ?>" />
		<meta name="geo.placename" content="<?php echo"$row_setting[geo_placename]"; ?>" />
		<meta name="geo.country" content="<?php echo"$row_setting[geo_country]"; ?>" />
		<meta name="geo.position" content="<?php echo"$row_setting[geo_position]"; ?>" />
		<meta name="Distribution" content="<?php echo"$row_setting[distribution]"; ?>">
		<meta name="ICBM" content="<?php echo"$row_setting[icbm]"; ?>" />

		<meta class="swiftype" name="last-updated" data-type="date" content="<?php echo"$last_updated"; ?>">
		<meta class="swiftype" name="meta-description" data-type="text" content="<?php echo"$row_setting[description]"; ?>">

		<meta name="twitter:card" content="<?php echo"$row_setting[twitter_card]"; ?>">
		<meta name="twitter:app:id:ipad" content="<?php echo"$row_setting[twitter_app_id_ipad]"; ?>">
		<meta name="twitter:app:id:iphone" content="<?php echo"$row_setting[twitter_app_id_iphone]"; ?>">
		<meta name="twitter:app:name:ipad" content="<?php echo"$row_setting[twitter_app_name_ipad]"; ?>">
		<meta name="twitter:app:name:iphone" content="<?php echo"$row_setting[twitter_app_name_iphone]"; ?>">
		<meta name="twitter:site" content="<?php echo"$row_setting[twitter_site]"; ?>">
		<meta name="twitter:creator" content="<?php echo"$row_setting[twitter_create]"; ?>">
		<meta name="twitter:title" content="<?php echo"$row_setting[twitter_title]"; ?>">
		<meta name="twitter:description" content="<?php echo"$row_setting[twitter_description]"; ?>">
		<meta name="twitter:image" content="<?php echo"$row_setting[twitter_image]"; ?>">
		<meta name="twitter:image:src" content="<?php echo"$row_setting[twitter_image_src]"; ?>">

		<meta name="google" content="<?php echo"$row_setting[title]"; ?>" />
		<meta name="googlebot" content="index, follow, all" />
		<meta name="googlebot-Image" content="index, follow, all" />
		<meta name="scooter" content="index, follow, all" />
		<meta name="msnbot" content="index, follow, all" />
		<meta name="teoma" content="index, follow, all" />
		<meta name="alexabot" content="index, follow, all" />
		<meta name="slurp" content="index, follow, all" />
		<meta name="zyBorg" content="index, follow, all" />
		<meta name="spiders" content="index, follow, all" />
		<meta name="robots" content="googlebot" />
		<meta name="robots" content="Googlebot-Image" />
		<meta name="robots" content="Scooter" />
		<meta name="robots" content="msnbot" />
		<meta name="robots" content="alexabot" />
		<meta name="robots" content="Slurp" />
		<meta name="robots" content="ZyBorg" />
		<meta name="robots" content="SPIDERS" />
		<meta name="robots" content="WEBCRAWLERS" />
		<meta name="robots" content="noodp" />
		<meta name="robots" content="noydir" />
		<meta name="MSSmartTagsPreventParsing" content="true" />
		<meta name="search engines" content="aeiwi, alexa, alltheWeb, altavista, aol netfind, anzwers, canada, directhit, euroseek, excite, overture, go, google, hotbot. infomak, kanoodle, lycos, mastersite, national directory, northern light, searchit, simplesearch, Websmostlinked, webtop, what-u-seek, aol, yahoo, webcrawler, infoseek, excite, magellan, looksmart, bing, cnet, googlebot" />

		<link rel="dns-prefetch" href="//apis.google.com">
		<link rel="dns-prefetch" href="//cdnjs.cloudflare.com" />
		<link rel="dns-prefetch" href="//fonts.googleapis.com" />
		<link rel="dns-prefetch" href="//fonts.gstatic.com">
		<link rel="dns-prefetch" href="//pixel.wp.com">
		<link rel="dns-prefetch" href="//s1.wp.com">
		<link rel="dns-prefetch" href="//s.w.org">
		<link rel="dns-prefetch" href="//stats.wp.com">
		<link rel="dns-prefetch" href="//ssl.google-analytics.com">

		<link rel="openid.server" href="<?php echo"$row_setting[openid_server]"; ?>/" />
		<link rel="openid.delegate" href="<?php echo"$row_setting[openid_delegate]"; ?>/" />

		<link rel="preconnect" href="//s1.wp.com" crossorigin="anonymous">
		<link rel="preconnect" href="//fonts.googleapis.com" crossorigin="anonymous">
		<link rel="preconnect" href="//fonts.gstatic.com" crossorigin="anonymous">
		<link rel="preconnect" href="//stats.wp.com" crossorigin="anonymous">
		<link rel="preconnect" href="//pixel.wp.com" crossorigin="anonymous">
		<link rel="preconnect" href="//ssl.google-analytics.com" crossorigin="anonymous">

		<link rel="profile" href="http://gmpg.org/xfn/11" />
		<link rel="author" href="<?php echo"$row_setting[link_author]"; ?>" />
		<link rel="publisher" href="<?php echo"$row_setting[link_publisher]"; ?>" />
		<link rel="EditURI" type="application/rsd+xml" title="<?php echo"$row_setting[title]"; ?> - RSD" href="<?php echo"$row_setting[domain]"; ?>/xmlrpc.php?rsd" />
		<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="<?php echo"$row_setting[domain]"; ?>/wlwmanifest.xml" />
		<link rel="apple-touch-icon-precomposed" href="<?php echo"$row_setting[domain]"; ?>/images/<?php echo"$row_setting[logo]"; ?>" />
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo"$row_setting[domain]"; ?>/images/<?php echo"$row_setting[favicon]"; ?>" />
		<link rel="shortlink" href="<?php echo"$row_setting[domain]"; ?>/">

		<link rel="search" type="application/opensearchdescription+xml" href="<?php echo"$row_setting[domain]"; ?>/osd.xml" title="<?php echo"$row_setting[title]"; ?>" />
		<link rel="search" type="application/opensearchdescription+xml" href="<?php echo"$row_setting[domain]"; ?>/opensearch.xml" title="<?php echo"$row_setting[title]"; ?>" />

		<link rel="alternate" hreflang="x-default" href="<?php echo"$row_setting[domain]"; ?>/" />
		<link rel="alternate" href="<?php echo"$row_setting[domain]"; ?>/atom.xml" title="<?php echo"$row_setting[title]"; ?> - ATOM" type="application/atom+xml"/>
		<link rel="alternate" href="<?php echo"$row_setting[domain]"; ?>" type="application/rss+xml" title="<?php echo"$row_setting[title]"; ?>" />
		<link rel="alternate" media="handheld" href="<?php echo"$row_setting[domain]"; ?>" />
		<link rel="alternate" media="only screen and (max-width: 640px)" href="<?php echo"$row_setting[domain]"; ?>/"/>
		<link rel="canonical" href="<?php echo"$row_setting[domain]"; ?><?php echo"$location"; ?>" />
		<link rel="pingback" href="<?php echo"$row_setting[domain]"; ?><?php echo"$location"; ?>" />
		
		
		<link href="<?php echo"$row_setting[domain]"; ?>/css/bootstrap.min.css" rel="stylesheet">
    	<link href="<?php echo"$row_setting[domain]"; ?>/css/prettyphoto.css" rel="stylesheet"> 
    	<link href="<?php echo"$row_setting[domain]"; ?>/css/font-awesome.min.css" rel="stylesheet"> 
    	<link href="<?php echo"$row_setting[domain]"; ?>/css/animate.css" rel="stylesheet"> 
    	<link href="<?php echo"$row_setting[domain]"; ?>/css/main.css" rel="stylesheet">
    	<link href="<?php echo"$row_setting[domain]"; ?>/css/responsive.css" rel="stylesheet"> 
    	<!--[if lt IE 9]> <script src="<?php echo"$row_setting[domain]"; ?>/js/html5shiv.js"></script> 
    	<script src="<?php echo"$row_setting[domain]"; ?>/js/respond.min.js"></script> <![endif]--> 
		
		
		<!-- Facebook Pixel Code for HubSpot CRM - Ben Ratner put this here -->
		<script>
			!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
			n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
			t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
			document,'script','//connect.facebook.net/en_US/fbevents.js');

			fbq('init', '<?php echo"$row_setting[fb_pages]"; ?>');
			fbq('track', 'PageView');
		</script>
		<noscript>&lt;img  height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=<?php echo"$row_setting[fb_pages]"; ?>&amp;amp;ev=PageView&amp;amp;noscript=1"&gt;</noscript>
		<!-- End HubSpot CRM Facebook Pixel Code -->


		<!-- Google Tracking and ID -->
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', '<?php echo"$row_setting[google_analytics_id]"; ?>', 'auto');
			ga('send', 'pageview');
		</script>

		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', '<?php echo"$row_setting[google_analytics_id]"; ?>']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
		</script>
		<!-- Google Tracking and ID -->


		<!--SQL Injection-->
		<script type="text/javascript" src="<?php echo"$row_setting[domain]"; ?>/sql-inject"></script>
		<!--SQL Injection-->


		<!-- Disable COPY SOURCE CODE (PASTE CAN = 86 and F12 DISABLE = 123) -->
		<script type="text/javascript">
		document.onkeydown = function(e) {
			event = (event || window.event);
			if (event.keyCode == 123) {
				return false;
			}
			if (e.ctrlKey && 
				(e.keyCode === 85 || 
				 e.keyCode === 117)) {
				/* alert('not allowed'); */
				return false;
			} else {
				return true;
			}
		};
		</script>
		<!-- Disable COPY SOURCE CODE (PASTE CAN = 86 and F12 DISABLE = 123) -->


		<!-- Disable IMAGE RIGHT CLICK -->
		<script>
		document.oncontextmenu = function(e){
			var target = (typeof e !="undefined")? e.target: event.srcElement
			if (target.tagName == "IMG" || (target.tagName == 'A' && target.firstChild.tagName == 'IMG'))
				return false
		}

		</script>
		<!-- Disable IMAGE RIGHT CLICK -->


		<!-- Disable Zoom -->
		<script>
			$(document).keydown(function(event) {
				if (event.ctrlKey==true && (event.which == '61' || event.which == '107' || event.which == '173' || event.which == '109'  || event.which == '187'  || event.which == '189'  ) ) {
					event.preventDefault();
				}
				// 107 Num Key  +
				// 109 Num Key  -
				// 173 Min Key  hyphen/underscor Hey
				// 61 Plus key  +/= key
			});

			$(window).bind('mousewheel DOMMouseScroll', function (event) {
				if (event.ctrlKey == true) {
					event.preventDefault();
				}
			});
		</script>
		<!-- Disable Zoom -->


		<!-- Disable Right Click -->
		<script language=JavaScript>
			var message="Function Disabled!";

			function clickIE4(){
				if (event.button==2){
					return false;
				}
			}

			function clickNS4(e){
				if (document.layers||document.getElementById&&!document.all){
					if (e.which==2||e.which==3){
					return false;
					}
				}
			}

			if (document.layers){
				document.captureEvents(Event.MOUSEDOWN);
				document.onmousedown=clickNS4;
			}
			else if (document.all&&!document.getElementById){
				document.onmousedown=clickIE4;
			}

			document.oncontextmenu=new Function("return false")

			// --> 
		</script>
		<!-- Disable Right Click -->
		
		
		<!--Jam dan waktu-->
		<script type="text/javascript">
			window.setTimeout("renderTanggal()",1);
			days = new Array(
				"Minggu,","Senin,","Selasa,","Rabu,","Kamis,","Jum'at,","Sabtu,"
			);
			months = new Array(
				"Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"
			);

			function renderTanggal(){
				var mydate = new Date();
				var year = mydate.getYear();
				if (year < 2000) {
					if (document.all)
						year = "19" + year;
					else
						year += 1900;
				}
				var day = mydate.getDay();
				var month = mydate.getMonth();
				var daym = mydate.getDate();
				if (daym < 10)
					daym = "0" + daym;
					var hours = mydate.getHours();
					var minutes = mydate.getMinutes();
					var seconds = mydate.getSeconds();
					var dn = "AM";
				if (hours >= 12) {
					dn = "PM";
					hours = hours - 12;
				}
				if (hours == 0)
					hours = 12;
					if (minutes <= 9)
						minutes = "0" + minutes;
				if (seconds <= 9)
					seconds = "0" + seconds;
					document.getElementById("jam").innerHTML = ""+days[day]+" "+daym+" "+months[month]+" "+year+" | "+hours+":"+minutes+":"+seconds+" "+dn+"";
					setTimeout("renderTanggal()",1000)
			}
			window.setTimeout("renderDate()",1);
			days = new Array(
				"Sunday,","Monday,","Tuesday,","Wednesday,","Thursday,","Friday,","Saturday,"
			);
			months = new Array(
				"January","February","March","April","May","June","July","August","September","October","November","December"
			);

			function renderDate(){
				var mydate = new Date();
				var year = mydate.getYear();
				if (year < 2000) {
					if (document.all)
						year = "19" + year;
					else
						year += 1900;
				}
				var day = mydate.getDay();
				var month = mydate.getMonth();
				var daym = mydate.getDate();
				if (daym < 10)
					daym = "0" + daym;
					var hours = mydate.getHours();
					var minutes = mydate.getMinutes();
					var seconds = mydate.getSeconds();
					var dn = "AM";
				if (hours >= 12) {
					dn = "PM";
					hours = hours - 12;
				}
				if (hours == 0)
					hours = 12;
					if (minutes <= 9)
						minutes = "0" + minutes;
				if (seconds <= 9)
					seconds = "0" + seconds;
					document.getElementById("hour").innerHTML = ""+days[day]+" "+months[month]+", "+daym+" "+year+" | "+hours+":"+minutes+":"+seconds+" "+dn+"";
					setTimeout("renderDate()",1000)
			}
		</script>
		<!--Jam dan waktu-->

		
		<!--Start of Tawk.to Script-->
        <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/59db76634854b82732ff47a1/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
        })();
        </script>
        <!--End of Tawk.to Script-->
		
		