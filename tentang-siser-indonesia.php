<?
include("connect_server.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php include("copyright.php"); ?>
<html lang="id" itemscope itemtype="http://schema.org/WebPage" xmlns="http://www.w3.org/1999/xhtml" xml:lang="id">
	<head>
		<title>Tentang Siser Indonesia | <?php echo"$row_setting[title]"; ?></title>
		
		<?php include("meta.php"); ?>
	</head>
	<body>
		
		<?php include("header.php"); ?>
	    
    	<section id="about-us" style="margin-top:50px; background-color:#FFFFFF;">
        	<div class="container">
    			<div class="col-sm-12">
    				<center><h2 class="title-one text-center">Mengapa Siser Indonesia?</h2></center>
    				<center><a draggable="false" href="http://www.siserasia-authorized.com/dealers/79921752.pdf"><img draggable="false" class="navbar-brand"><h1><img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/siserdistributorresmi-new.png" width="550" alt="Logo <?php echo"$row_setting[name_website]"; ?> - <?php echo"$row_setting[title]"; ?>"></a></center><br><br><br>
    				<p style="text-align:justify;">
    				    Siser Indonesia adalah website screen printing di indonesia yang menyediakan Cutting Material dan Printing Material dengan produk terbaik dan kualitas teratas untuk memenuhi kebutuhan para pencetak sablon di berbagai aplikasi dan industri seperti PCB, Elektronik, Grafik/Poster, Garmen/Tekstil, Plastik, Keramik transfer/decal, Stiker, Sepatu, Spanduk, dll.<br>
    				    <br>
                        Berbahan thermo transfer vinyl (sticker) untuk diaplikasikan pada tekstil dan apparel, pada umumnya disebut stiker Polyflex. Siser adalah brand Itali yang secara internasional diakui sebagai bahan yang handal, mampu menempel dengan kuat, melalui proses pengujian yang ketat serta mudah digunakan. Dan Siser Indonesia kami terhubung dengan CENTRAL SPS yang merupakan salah satu perusahaan Screen Printing Terbesar di Indonesia yang saat ini hampir memasuki usia 4 dasawarsa. Berdiri sejak tahun 1974 dengan kantor pusat di Jakarta dan telah memiliki berbagai cabang yang tersebar di seluruh wilayah Indonesia. Selama perjalanannya hingga saat ini, CENTRAL SPS telah meraih banyak rekor yang membanggakan di berbagai event Internasional dan mendapat berbagai penghargaan serta medali dari berbagai ajang kompetisi. Pencapaian ini sangat mengharumkan nama Indonesia dalam bidang Screen Printing.<br>
                        <br>
                        Pada decade 1990-an, CENTRAL SPS juga sukses menciptakan teknologi baru di kain screen, tinta maupun emulsi dimana hal tersebut telah disebar-luaskan melalui penyelenggaraan seminar-seminar, technical training, dan workshop di berbagai kota di Indonesia, China maupun Vietnam. Salah satu kontribusi CENTRAL SPS dalam perkembangan Screen Printing adalah dalam proses cetak 4 warna (halftone) untuk cetak poster dan “ batik motif “ modern, yang mana hal tersebut sangat berpengaruh dalam industri Screen Printing Indonesia sampai saat ini.<br>
                        <br>
                        Sebagai pemegang Distributor berbagai merk Internasional, CENTRAL SPS mempunyai jaringan luas dan hubungan yang sangat baik dengan principals dan asosiasi-asosiasi berrbagai Negara di Eropa, USA maupun Asia Pacific. Kemampuan untuk selalu mengembangkan inovasi-inovasi baru, meluncurkan produk yang ramah lingkungan, mengutamakan kualitas produk dan layanan terbaik, menempatkan CENTRAL SPS menjadi “The Leader” di bidang industry Screen Printing.<br>
                        <br>
                        Dengan jaminan bahwa produk kami adalah ORIGINAL dan sebagai pemegang Distributor dari ATMA, EPI INKS, ORTECH, ULANO, ACLA, VS-MONOPRINT, POLYONE, UNITEX, ALTA, APOLLO, FIMOR, EPIRACK, FURUKAWA, dll, maka CENTRAL SPS dengan bangga memberikan produk terbaik dan berkualitas untuk memenuhi kebutuhan para pencetak sablon di berbagai aplikasi dan industri seperti PCB, Elektronik, Grafik/Poster, Garmen/Tekstil, Plastik, Keramik transfer/decal, Stiker, Sepatu, Spanduk, dll. Selain melayani pemasangan Screen Komplit berbagai ukuran dengan jenis bingkai aluminium yang bermutu dan berbagai macam pilihan profile ataupun dengan bingkai kayu yang berkualitas, CENTRAL SPS juga memberikan konsultasi penggunaan berbagai tinta secara tepat dan benar dengan dukungan Laboratorium yang lengkap.
    				</p>
        		</div>
    		</div>
    	</section>
        
		<section id="portfolio" style="background-color:#DCDCDC;">
			<div class="container">
				<div class="row text-center">
					<div class="col-sm-8 col-sm-offset-2">
						<h2 class="title-one">Galeri Siser Indonesia</h2>
						<p>Berikut ini adalah galeri Siser Indonesia</p>
					</div>
				</div>
				<div class="portfolio-items">
				<?
				$a=1;
				while($a <= 21)
				{
				?>
					<div class="col-sm-3 col-xs-12 portfolio-item html">
						<div class="portfolio-image">
							<img src="images/galeri/galeri-<?php echo"$a"; ?>.jpg" alt="">
						</div>
					</div>
				<?
				    $a++;
				}
				?>
				</div>
			</div> 
		</section>
		
    	<?php include("footer.php"); ?>
    </body>
</html>