<?
include("connect_server.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php include("copyright.php"); ?>
<html lang="id" itemscope itemtype="http://schema.org/WebPage" xmlns="http://www.w3.org/1999/xhtml" xml:lang="id">
	<head>
		<title>Cari | <?php echo"$row_setting[title]"; ?></title>
		
		<?php include("meta.php"); ?>
	</head>
	<body>
		
		<?php include("header.php"); ?>
		
		<section id="blog" style="margin-top:50px;"> 
			<div class="container">
				<div class="row text-center clearfix">
					<div class="col-sm-8 col-sm-offset-2">
						<h2 class="title-one">Cari</h2>
						<p class="blog-heading">Berikut Ini Hasil Pencarian <strong>"<?php echo"$_POST[cari]"; ?>"</strong></p>
					</div>
				</div>
				<div class="row">
				<?
        		$result_material = mysql_query("SELECT * FROM material WHERE nama_material LIKE '%$_POST[cari]%' ORDER BY id_material DESC");
        		while($row_material = mysql_fetch_array($result_material))
        		{
				?>
					<div class="col-sm-3">
						<div class="single-blog">
							<img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/material/<?php echo"$row_material[gambar_material]"; ?>" alt="Material <?php echo"$row_material[nama_material]"; ?> - <?php echo"$row_setting[title]"; ?>" />
							<h2><?php echo"$row_material[nama_material]"; ?></h2>
							<div class="blog-content">
								<p style="white-space:pre-wrap; text-align:justify;"><?php echo substr("$row_material[deskripsi_material]",0,120); ?>&hellip;</p>
							</div>
							<a draggable="false" href="" class="btn btn-primary" data-toggle="modal" data-target="#material-<?php echo"$row_material[id_material]"; ?>">Lihat Rincian</a>
						</div>
						<div class="modal fade" id="material-<?php echo"$row_material[id_material]"; ?>" tabindex="-1" role="dialog" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-body">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										<img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/material/<?php echo"$row_material[gambar_material]"; ?>" alt="Material <?php echo"$row_material[nama_material]"; ?> - <?php echo"$row_setting[title]"; ?>" />
										<h2><?php echo"$row_material[nama_material]"; ?></h2>
										<p style="white-space:pre-wrap; text-align:justify;"><?php echo"$row_material[deskripsi_material]"; ?></p><br>
										<table id="dynamic-table" class="table table-striped table-bordered table-hover">
										    <tr>
										        <th colspan="4">DATA TEKNIS</th>
										    </tr>
										    <tr>
										        <th colspan="4">Intruksi Aplikasi</th>
										    </tr>
										    <tr>
										        <td style="width:10%;"><img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/material/icon-intruksi-aplikasi-1.png"></td>
										        <td style="width:40%;"><p style="white-space:pre-wrap; text-align:justify;"><?php echo"$row_material[intruksi_aplikasi_1_material]"; ?></p></td>
										        <td style="width:10%;"><img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/material/icon-intruksi-aplikasi-2.png"></td>
										        <td style="width:40%;"><p style="white-space:pre-wrap; text-align:justify;"><?php echo"$row_material[intruksi_aplikasi_2_material]"; ?></p></td>
										    </tr>
										    <tr>
										        <td style="width:10%;"><img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/material/icon-intruksi-aplikasi-3.png"></td>
										        <td style="width:40%;"><p style="white-space:pre-wrap; text-align:justify;"><?php echo"$row_material[intruksi_aplikasi_3_material]"; ?></p></td>
										        <td style="width:10%;"><img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/material/icon-intruksi-aplikasi-4.png"></td>
										        <td style="width:40%;"><p style="white-space:pre-wrap; text-align:justify;"><?php echo"$row_material[intruksi_aplikasi_4_material]"; ?></p></td>
										    </tr>
										    <tr>
										        <th colspan="4">Petunjuk Mencuci</th>
										    </tr>
										    <tr>
										        <td style="width:10%;"><img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/material/icon-petunjuk-mencuci-1.png"></td>
										        <td style="width:40%;"><p style="white-space:pre-wrap; text-align:justify;"><?php echo"$row_material[petunjuk_mencuci_1_material]"; ?></p></td>
										        <td style="width:10%;"><img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/material/icon-petunjuk-mencuci-2.png"></td>
										        <td style="width:40%;"><p style="white-space:pre-wrap; text-align:justify;"><?php echo"$row_material[petunjuk_mencuci_2_material]"; ?></p></td>
										    </tr>
										    <tr>
										        <td style="width:10%;"><img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/material/icon-petunjuk-mencuci-3.png"></td>
										        <td style="width:40%;"><p style="white-space:pre-wrap; text-align:justify;"><?php echo"$row_material[petunjuk_mencuci_3_material]"; ?></p></td>
										        <td style="width:10%;"><img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/material/icon-petunjuk-mencuci-4.png"></td>
										        <td style="width:40%;"><p style="white-space:pre-wrap; text-align:justify;"><?php echo"$row_material[petunjuk_mencuci_4_material]"; ?></p></td>
										    </tr>
										    <tr>
										        <td style="width:10%;"><img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/material/icon-petunjuk-mencuci-5.png"></td>
										        <td style="width:90%;" colspan="3"><p style="white-space:pre-wrap; text-align:justify;"><?php echo"$row_material[petunjuk_mencuci_5_material]"; ?></p></td>
										    </tr>
										    <tr>
										        <th colspan="4">Deskripsi</th>
										    </tr>
										    <tr>
										        <td style="width:10%;"><img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/material/icon-deskripsi-1.png"></td>
										        <td style="width:40%;"><p style="white-space:pre-wrap; text-align:justify;"><?php echo"$row_material[deskripsi_1_material]"; ?></p></td>
										        <td style="width:10%;"><img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/material/icon-deskripsi-2.png"></td>
										        <td style="width:40%;"><p style="white-space:pre-wrap; text-align:justify;"><?php echo"$row_material[deskripsi_2_material]"; ?></p></td>
										    </tr>
										    <tr>
										        <td style="width:10%;"><img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/material/icon-deskripsi-3.png"></td>
										        <td style="width:40%;"><p style="white-space:pre-wrap; text-align:justify;"><?php echo"$row_material[deskripsi_3_material]"; ?></p></td>
										        <td style="width:10%;"><img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/material/icon-deskripsi-4.png"></td>
										        <td style="width:40%;"><p style="white-space:pre-wrap; text-align:justify;"><?php echo"$row_material[deskripsi_4_material]"; ?></p></td>
										    </tr>
										    <tr>
										        <th colspan="4">Tekstil Yang Cocok</th>
										    </tr>
										    <tr>
										        <td colspan="4"><p style="white-space:pre-wrap; text-align:justify;"><?php echo"$row_material[tekstil_material]"; ?></p></td>
										    </tr>
										</table>
									</div> 
								</div>
							</div>
						</div>
					</div>
				<?
				}
				?>
				</div> 
			</div> 
		</section>
		
		<?php include("footer.php"); ?>
	</body>
</html>