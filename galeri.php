<?
include("connect_server.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php include("copyright.php"); ?>
<html lang="id" itemscope itemtype="http://schema.org/WebPage" xmlns="http://www.w3.org/1999/xhtml" xml:lang="id">
	<head>
		<title>Galeri | <?php echo"$row_setting[title]"; ?></title>
		
		<?php include("meta.php"); ?>
	</head>
	<body>
		
		<?php include("header.php"); ?>
	    
    	<section id="about-us" style="margin-top:50px;">
        	<div class="container">
    			<div class="col-sm-12">
    				<center><h2 class="title-one text-center">Galeri</h2></center>
    				<div class="row">
    					<div class="col-sm-6">
    						<div class="single-blog">
    							<div class="embed-responsive embed-responsive-16by9">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/4gucDB4oxi0" style="width:100%; height:300px;" allowfullscreen></iframe>
                                </div>
                                <br>
    							<h5>Did You Know? You Can Sublimate on Siser Glitter!</h5>
    						</div>
    					</div>
    					<div class="col-sm-6">
    						<div class="single-blog">
    							<div class="embed-responsive embed-responsive-16by9">
                                    <video width="100%" controls style="width:100%; height: 300px;" allowfullscreen>
                                        <source src="https://www.siserindonesia.com/video/creating-a-distressed-look-with-siser-heat-transfer-vinyl-and-a-vinyl-cutter.mp4" type="video/mp4">
                                        Your browser does not support HTML5 video.
                                    </video>
                                </div>
                                <br>
    							<h5>Creating A Distressed Look With Siser Heat Transfer Vinyl And A Vinyl Cutter</h5>
    						</div>
    					</div>
    					<div class="col-sm-6">
    						<div class="single-blog">
    							<div class="embed-responsive embed-responsive-16by9">
                                    <video width="100%" controls style="width:100%; height: 300px;" allowfullscreen>
                                        <source src="https://www.siserindonesia.com/video/customizing-a-basketball-with-siser-htv.mp4" type="video/mp4">
                                        Your browser does not support HTML5 video.
                                    </video>
                                </div>
                                <br>
    							<h5>Customizing a Basketball with Siser™ HTV</h5>
    						</div>
    					</div>
    					<div class="col-sm-6">
    						<div class="single-blog">
    							<div class="embed-responsive embed-responsive-16by9">
                                    <video width="100%" controls style="width:100%; height: 300px;" allowfullscreen>
                                        <source src="https://www.siserindonesia.com/video/digital-transfer-for-technology.mp4" type="video/mp4">
                                        Your browser does not support HTML5 video.
                                    </video>
                                </div>
                                <br>
    							<h5>Digital Transfer For Technology</h5>
    						</div>
    					</div>
    					<div class="col-sm-6">
    						<div class="single-blog">
    							<div class="embed-responsive embed-responsive-16by9">
                                    <video width="100%" controls style="width:100%; height: 300px;" allowfullscreen>
                                        <source src="https://www.siserindonesia.com/video/how-to-apply-colorprint-easy-by-siser.mp4" type="video/mp4">
                                        Your browser does not support HTML5 video.
                                    </video>
                                </div>
                                <br>
    							<h5>How To Apply Colorprint Easy By Siser</h5>
    						</div>
    					</div>
    					<div class="col-sm-6">
    						<div class="single-blog">
    							<div class="embed-responsive embed-responsive-16by9">
                                    <video width="100%" controls style="width:100%; height: 300px;" allowfullscreen>
                                        <source src="https://www.siserindonesia.com/video/siser-3d.mp4" type="video/mp4">
                                        Your browser does not support HTML5 video.
                                    </video>
                                </div>
                                <br>
    							<h5>Siser 3D</h5>
    						</div>
    					</div>
    					<div class="col-sm-6">
    						<div class="single-blog">
    							<div class="embed-responsive embed-responsive-16by9">
                                    <video width="100%" controls style="width:100%; height: 300px;" allowfullscreen>
                                        <source src="https://www.siserindonesia.com/video/how-to-heat-press-siser-glitter.mp4" type="video/mp4">
                                        Your browser does not support HTML5 video.
                                    </video>
                                </div>
                                <br>
    							<h5>How to Heat Press Siser Glitter</h5>
    						</div>
    					</div>
    					<div class="col-sm-6">
    						<div class="single-blog">
    							<div class="embed-responsive embed-responsive-16by9">
                                    <video width="100%" controls style="width:100%; height: 300px;" allowfullscreen>
                                        <source src="https://www.siserindonesia.com/video/how-to-apply-siser-easyweed-on-a-t-shirt.mp4" type="video/mp4">
                                        Your browser does not support HTML5 video.
                                    </video>
                                </div>
                                <br>
    							<h5>How To Apply Siser EasyWeed On A T-Shirt!</h5>
    						</div>
    					</div>
    					<div class="col-sm-6">
    						<div class="single-blog">
    							<div class="embed-responsive embed-responsive-16by9">
                                    <video width="100%" controls style="width:100%; height: 300px;" allowfullscreen>
                                        <source src="https://www.siserindonesia.com/video/how-to-apply-siser-brick-heat-transfer-vinyl.mp4" type="video/mp4">
                                        Your browser does not support HTML5 video.
                                    </video>
                                </div>
                                <br>
    							<h5>How To Apply Siser Brick Heat Transfer Vinyl</h5>
    						</div>
    					</div>
    					<div class="col-sm-6">
    						<div class="single-blog">
    							<div class="embed-responsive embed-responsive-16by9">
                                    <video width="100%" controls style="width:100%; height: 300px;" allowfullscreen>
                                        <source src="https://www.siserindonesia.com/video/how-to-layer-heat-transfer-vinyl-step-by-step-instructions.mp4" type="video/mp4">
                                        Your browser does not support HTML5 video.
                                    </video>
                                </div>
                                <br>
    							<h5>How To Layer Heat Transfer Vinyl - Step by Step Instructions</h5>
    						</div>
    					</div>
    					<div class="col-sm-6">
    						<div class="single-blog">
    							<div class="embed-responsive embed-responsive-16by9">
                                    <video width="100%" controls style="width:100%; height: 300px;" allowfullscreen>
                                        <source src="https://www.siserindonesia.com/video/tips-and-tricks-for-applying-siser-stripflock.mp4" type="video/mp4">
                                        Your browser does not support HTML5 video.
                                    </video>
                                </div>
                                <br>
    							<h5>Tips and Tricks for Applying Siser StripFlock</h5>
    						</div>
    					</div>
    					<div class="col-sm-6">
    						<div class="single-blog">
    							<div class="embed-responsive embed-responsive-16by9">
                                    <video width="100%" controls style="width:100%; height: 300px;" allowfullscreen>
                                        <source src="https://www.siserindonesia.com/video/siser-easy-weed-heat-transfer-vinyl.mp4" type="video/mp4">
                                        Your browser does not support HTML5 video.
                                    </video>
                                </div>
                                <br>
    							<h5>Siser Easy Weed Heat Transfer Vinyl.mp4</h5>
    						</div>
    					</div>
    					<div class="col-sm-6">
    						<div class="single-blog">
    							<div class="embed-responsive embed-responsive-16by9">
                                    <video width="100%" controls style="width:100%; height: 300px;" allowfullscreen>
                                        <source src="https://www.siserindonesia.com/video/siser-easyweed-foil.mp4" type="video/mp4">
                                        Your browser does not support HTML5 video.
                                    </video>
                                </div>
                                <br>
    							<h5>Siser Easyweed Foil.mp4</h5>
    						</div>
    					</div>
    					<div class="col-sm-6">
    						<div class="single-blog">
    							<div class="embed-responsive embed-responsive-16by9">
                                    <video width="100%" controls style="width:100%; height: 300px;" allowfullscreen>
                                        <source src="https://www.siserindonesia.com/video/how-to-weed-mask-and-apply-colorPrint-easy.mp4" type="video/mp4">
                                        Your browser does not support HTML5 video.
                                    </video>
                                </div>
                                <br>
    							<h5>How to Weed, Mask and Apply ColorPrint Easy.mp4</h5>
    						</div>
    					</div>
    					<div class="col-sm-6">
    						<div class="single-blog">
    							<div class="embed-responsive embed-responsive-16by9">
                                    <video width="100%" controls style="width:100%; height: 300px;" allowfullscreen>
                                        <source src="https://www.siserindonesia.com/video/how-to-mask-and-apply-colorprint-pu-print-and-cut-material-on-t-shirts.mp4" type="video/mp4">
                                        Your browser does not support HTML5 video.
                                    </video>
                                </div>
                                <br>
    							<h5>How to mask and apply ColorPrint PU print and cut material on T-shirts.mp4</h5>
    						</div>
    					</div>
    					<div class="col-sm-6">
    						<div class="single-blog">
    							<div class="embed-responsive embed-responsive-16by9">
                                    <video width="100%" controls style="width:100%; height: 300px;" allowfullscreen>
                                        <source src="https://www.siserindonesia.com/video/how-to-make-an-awesome-custom-hat-with-an-otto-hat-and-siser-brick-htv.mp4" type="video/mp4">
                                        Your browser does not support HTML5 video.
                                    </video>
                                </div>
                                <br>
    							<h5>How to make an Awesome Custom Hat with an Otto Hat and Siser Brick HTV.mp4</h5>
    						</div>
    					</div>
    					
    				</div>
        		</div>
    		</div>
    	</section>
        
		<section id="portfolio" style="background-color:#FFFFFF;">
			<div class="container">
				<div class="row text-center">
					<div class="col-sm-8 col-sm-offset-2">
						<h2 class="title-one">Galeri Siser Indonesia</h2>
						<p>Berikut ini adalah galeri Siser Indonesia</p>
					</div>
				</div>
				<div class="portfolio-items">
				<?
				$a=1;
				while($a <= 21)
				{
				?>
					<div class="col-sm-3 col-xs-12 portfolio-item html">
						<div class="portfolio-image">
							<img src="images/galeri/galeri-<?php echo"$a"; ?>.jpg" alt="">
						</div>
					</div>
				<?
				    $a++;
				}
				?>
				</div>
			</div> 
		</section>
		
    	<?php include("footer.php"); ?>
    </body>
</html>