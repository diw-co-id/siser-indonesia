		<!-- Vendor -->
		<script src="<?php echo"$row_setting[domain]"; ?>/vendor/jquery/jquery.min.js"></script>
		<script src="<?php echo"$row_setting[domain]"; ?>/vendor/jquery.appear/jquery.appear.min.js"></script>
		<script src="<?php echo"$row_setting[domain]"; ?>/vendor/jquery.easing/jquery.easing.min.js"></script>
		<script src="<?php echo"$row_setting[domain]"; ?>/vendor/jquery-cookie/jquery-cookie.min.js"></script>
		<!--<script src="<?php echo"$row_setting[domain]"; ?>/master/style-switcher/style.switcher.js" id="styleSwitcherScript" data-base-path="<?php echo"$row_setting[domain]"; ?>/" data-skin-src="<?php echo"$row_setting[domain]"; ?>/"></script>-->
		<script src="<?php echo"$row_setting[domain]"; ?>/vendor/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?php echo"$row_setting[domain]"; ?>/vendor/common/common.min.js"></script>
		<script src="<?php echo"$row_setting[domain]"; ?>/vendor/jquery.validation/jquery.validation.min.js"></script>
		<script src="<?php echo"$row_setting[domain]"; ?>/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<script src="<?php echo"$row_setting[domain]"; ?>/vendor/jquery.gmap/jquery.gmap.min.js"></script>
		<script src="<?php echo"$row_setting[domain]"; ?>/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
		<script src="<?php echo"$row_setting[domain]"; ?>/vendor/isotope/jquery.isotope.min.js"></script>
		<script src="<?php echo"$row_setting[domain]"; ?>/vendor/owl.carousel/owl.carousel.min.js"></script>
		<script src="<?php echo"$row_setting[domain]"; ?>/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
		<script src="<?php echo"$row_setting[domain]"; ?>/vendor/vide/vide.min.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo"$row_setting[domain]"; ?>/js/theme.js"></script>

		<script src="<?php echo"$row_setting[domain]"; ?>/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script src="<?php echo"$row_setting[domain]"; ?>/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

		<!-- Current Page Vendor and Views -->
		<script src="<?php echo"$row_setting[domain]"; ?>/js/views/view.contact.js"></script>	
		<script src="<?php echo"$row_setting[domain]"; ?>/vendor/circle-flip-slideshow/js/jquery.flipshow.min.js"></script>		
		<script src="<?php echo"$row_setting[domain]"; ?>/js/views/view.home.js"></script>
		
		<!-- Demo -->
		<script src="<?php echo"$row_setting[domain]"; ?>/js/demos/demo-shop-5.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo"$row_setting[domain]"; ?>/js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo"$row_setting[domain]"; ?>/js/theme.init.js"></script>
		<script src="<?php echo"$row_setting[domain]"; ?>/master/analytics/analytics.js"></script>
		
		<a href="https://www.old.diw.co.id/" class="go-to-demos" target="_blank"><b><i class="fa fa-globe fa-fw"></i> Go To Old Version</b></a>
		<!--<a href="https://www.old.diw.co.id/" class="go-to-demos" style="margin-top:100px;" target="_blank"><i class="fa fa-globe fa-fw"></i> Go To Old Version 2</a>-->