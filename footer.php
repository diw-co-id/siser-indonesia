		<footer id="footer" style="background-color:#117747;"> 
			<div class="container"> 
				<div class="text-center"> 
					<p style="color:#FFFFFF;">&copy; Hak Cipta <?php echo date("Y"); ?> &reg; <a draggable="false" href="<?php echo"$row_setting[domain]"; ?>/" target="_blank"><strong style="color:#FFFFFF;"><?php echo"$row_setting[name_website]"; ?></strong></a> &trade;. Seluruh Hak Cipta Dilindungi.</p> 
				</div> 
			</div> 
		</footer>
	
		<script type="text/javascript" src="<?php echo"$row_setting[domain]"; ?>/js/jquery.js"></script> 
		<script type="text/javascript" src="<?php echo"$row_setting[domain]"; ?>/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo"$row_setting[domain]"; ?>/js/smoothscroll.js"></script> 
		<script type="text/javascript" src="<?php echo"$row_setting[domain]"; ?>/js/jquery.isotope.min.js"></script>
		<script type="text/javascript" src="<?php echo"$row_setting[domain]"; ?>/js/jquery.prettyPhoto.js"></script> 
		<script type="text/javascript" src="<?php echo"$row_setting[domain]"; ?>/js/jquery.parallax.js"></script> 
		<script type="text/javascript" src="<?php echo"$row_setting[domain]"; ?>/js/main.js"></script> 