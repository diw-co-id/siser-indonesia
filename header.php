        <div class="preloader">
    		<div class="preloder-wrap">
    			<div class="preloder-inner">
    				<div class="ball"></div>
    				<div class="ball"></div>
    				<div class="ball"></div>
    				<div class="ball"></div>
    				<div class="ball"></div>
    				<div class="ball"></div>
    				<div class="ball"></div>
    			</div>
    		</div>
    	</div>
    	
    	<header id="navigation">
    		<div class="navbar navbar-inverse navbar-fixed-top" role="banner">
    			<div class="container">
    				<div class="navbar-header">
    					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
    						<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
    					</button>
    					<a draggable="false" class="navbar-brand" href="http://www.siserasia-authorized.com/dealers/79921752.pdf"><h1><img draggable="false" style="width:150px;" src="<?php echo"$row_setting[domain]"; ?>/images/siserdistributorresmi-new.png" alt="Logo <?php echo"$row_setting[name_website]"; ?> - <?php echo"$row_setting[title]"; ?>"></h1></a>
    				</div>
    				<div class="collapse navbar-collapse"> 
    					<ul class="nav navbar-nav navbar-right">
    						<li class="scroll <?php echo"$beranda_active"; ?>"><a draggable="false" href="<?php echo"$row_setting[domain]"; ?>/"><i class="fa fa-home fa-fw" aria-hidden="true"></i> Home</a></li>
    						<li class="scroll <?php echo"$tentang_siser_indonesia_active"; ?>"><a draggable="false" href="<?php echo"$row_setting[domain]"; ?>/tentang-siser-indonesia"><i class="fa fa-building-o fa-fw" aria-hidden="true"></i> Tentang Kami</a></li>
    						<li class="scroll <?php echo"$faq_active"; ?>"><a draggable="false" href="<?php echo"$row_setting[domain]"; ?>/faq"><i class="fa fa-question-circle fa-fw" aria-hidden="true"></i> FAQ</a></li>
    						<li class="scroll <?php echo"$galeri_active"; ?>"><a draggable="false" href="<?php echo"$row_setting[domain]"; ?>/galeri"><i class="fa fa-picture-o fa-fw" aria-hidden="true"></i> Galeri</a></li>
    						<li class="scroll <?php echo"$cutting_material_active"; ?>"><a draggable="false" href="<?php echo"$row_setting[domain]"; ?>/cutting-material"><i class="fa fa-scissors fa-fw" aria-hidden="true"></i> Cutting Material</a></li>
    						<li class="scroll <?php echo"$printing_material_active"; ?>"><a draggable="false" href="<?php echo"$row_setting[domain]"; ?>/printing-material"><i class="fa fa-print fa-fw" aria-hidden="true"></i> Printing Material</a></li>
    						<li class="scroll <?php echo"$hubungi_kami_active"; ?>"><a draggable="false" href="<?php echo"$row_setting[domain]"; ?>/hubungi-kami"><i class="fa fa-phone fa-fw" aria-hidden="true"></i> Hubungi Kami</a></li>
    						<li class="scroll" style="margin-top:-8px; margin-left:15px;">
    						    <form class="navbar-form" role="search" action="<?php echo"$row_setting[domain]"; ?>/cari" method="post">
                                    <input class="form-control" style="width:120px; float:left;" placeholder="Cari..." name="cari" id="cari" type="text" list="browsers" value="<?php echo"$_POST[cari]"; ?>" required>
                                    <datalist id="browsers">
                    				<?
                            		$result_material = mysql_query("SELECT * FROM material ORDER BY nama_material ASC");
                            		while($row_material = mysql_fetch_array($result_material))
                            		{
                    				?>
                                        <option value="<?php echo"$row_material[nama_material]"; ?>">
                                    <?
                            		}
                            		?>
                                    </datalist>
                                    <button class="btn btn-default" name="button_search" type="submit" style="float:left;"><i class="fa fa-search" style="height:16px; margin-top:4px;"></i></button><br><br>
                                </form>
    						</li>
    					</ul>
    				</div>
    			</div>
    		</div>
    	</header>