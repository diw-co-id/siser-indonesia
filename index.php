<?
include("connect_server.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php include("copyright.php"); ?>
<html lang="id" itemscope itemtype="http://schema.org/WebPage" xmlns="http://www.w3.org/1999/xhtml" xml:lang="id">
	<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title><?php echo"$row_setting[title]"; ?></title>
		
		<?php include("meta.php"); ?>
	</head>
	<body>
			
		<?php include("header.php"); ?>
	
    	<section id="home">
    		<div class="home-pattern"></div>
    		<div id="main-carousel" class="carousel slide" data-ride="carousel">
    			<ol class="carousel-indicators">
    				<li data-target="#main-carousel" data-slide-to="0" class="active"></li>
    				<li data-target="#main-carousel" data-slide-to="1"></li>
    				<li data-target="#main-carousel" data-slide-to="2"></li>
    				<li data-target="#main-carousel" data-slide-to="3"></li>
    			</ol>
    			<div class="carousel-inner">
    				<div class="item active" style="background-image: url(<?php echo"$row_setting[domain]"; ?>/images/slider/slider-4.jpg)">
    					<div class="carousel-caption">
    						<div>
    							<h2 class="heading animated bounceInDown" style="text-shadow: 2px 2px 2px black;">Selamat Datang di Website Siser Indonesia</h2>
    							<p class="animated bounceInUp" style="text-shadow: 2px 2px 2px black;">Siser Indonesia adalah website screen printing di indonesia yang menyediakan Cutting Material dan Printing Material</p>
    							<a draggable="false" class="btn btn-default slider-btn animated fadeIn" href="<?php echo"$row_setting[domain]"; ?>/tentang-siser-indonesia">Baca Selengkapnya</a>
    						</div>
    					</div>
    				</div>
    				<div class="item" style="background-image: url(<?php echo"$row_setting[domain]"; ?>/images/slider/slider-3.jpg)">
    					<div class="carousel-caption">
    						<div>
    							<h2 class="heading animated bounceInDown" style="text-shadow: 2px 2px 2px black;">Did You Know? You Can Sublimate on Siser Glitter!</h2>
    							<p class="animated bounceInUp" style="text-shadow: 2px 2px 2px black;">Dengan Kualitas Terbaik Dan Produk Terbaik Di Kelasnya</p>
    							<a draggable="false" class="btn btn-default slider-btn animated fadeIn" href="<?php echo"$row_setting[domain]"; ?>/galeri">Lihat Video</a>
    						</div>
    					</div>
        			</div>
    				<div class="item" style="background-image: url(<?php echo"$row_setting[domain]"; ?>/images/slider/slider-2.jpg)">
    					<div class="carousel-caption">
    						<div>
    							<h2 class="heading animated bounceInDown" style="text-shadow: 2px 2px 2px black;">Frequently Asked Question</h2>
    							<p class="animated bounceInUp" style="text-shadow: 2px 2px 2px black;">Pertanyaan yang sering diajukan pada Siser Indonesia</p>
    							<a draggable="false" class="btn btn-default slider-btn animated fadeIn" href="<?php echo"$row_setting[domain]"; ?>/faq">Baca FAQ</a>
    						</div>
    					</div>
    				</div>
        			<div class="item" style="background-image: url(<?php echo"$row_setting[domain]"; ?>/images/slider/slider-1.jpg)">
        				<div class="carousel-caption">
    						<div>
    							<h2 class="heading animated bounceInDown" style="text-shadow: 2px 2px 2px black;">Anda Dapat Menghubungi Kami Kapanpun</h2>
    							<p class="animated bounceInUp" style="text-shadow: 2px 2px 2px black;">Hubungi Kami Jika Ada Pertanyaan Dan Keluhan Anda</p>
    							<a draggable="false" class="btn btn-default slider-btn animated fadeIn" href="<?php echo"$row_setting[domain]"; ?>/hubungi-kami">Hubungi Kami Sekarang</a>
    						</div>
    					</div>
        			</div>
        		</div>
    
        		<a draggable="false" class="carousel-left member-carousel-control hidden-xs" href="#main-carousel" data-slide="prev"><i class="fa fa-angle-left"></i></a>
        		<a draggable="false" class="carousel-right member-carousel-control hidden-xs" href="#main-carousel" data-slide="next"><i class="fa fa-angle-right"></i></a>
        	</div> 
    
        </section>
        
        <section id="about-us">
        	<div class="container">
        		<div class="text-center">
        			<div class="col-sm-8 col-sm-offset-2">
        				<h2 class="title-one">Mengapa Siser Indonesia?</h2>
        				<img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/logo-siser-indonesia.png" width="300" alt="Logo <?php echo"$row_setting[name_website]"; ?> - <?php echo"$row_setting[title]"; ?>"><br><br><br>
        				<p>Siser Indonesia adalah website screen printing di indonesia yang menyediakan Cutting Material dan Printing Material dengan produk terbaik dan kualitas teratas untuk memenuhi kebutuhan para pencetak sablon di berbagai aplikasi dan industri seperti PCB, Elektronik, Grafik/Poster, Garmen/Tekstil, Plastik, Keramik transfer/decal, Stiker, Sepatu, Spanduk, dll.</p>
        			</div>
        		</div>
    		</div>
    	</section>
        
		<section id="portfolio" style="background-color:#FFFFFF;">
			<div class="container">
				<div class="row text-center">
					<div class="col-sm-8 col-sm-offset-2">
						<h2 class="title-one">Galeri Siser Indonesia</h2>
						<p>Berikut ini adalah galeri Siser Indonesia</p>
					</div>
				</div>
				<div class="portfolio-items">
				<?
				$a=1;
				while($a <= 21)
				{
				?>
					<div class="col-sm-3 col-xs-12 portfolio-item html">
						<div class="portfolio-image">
							<img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/galeri/galeri-<?php echo"$a"; ?>.jpg" alt="Galeri <?php echo"$a"; ?> - <?php echo"$row_setting[title]"; ?>">
						</div>
					</div>
				<?
				    $a++;
				}
				?>
				</div>
			</div> 
		</section>
        					
    	<?php include("footer.php"); ?>
    </body>
</html>